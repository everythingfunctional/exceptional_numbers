module exceptional_numbers
    implicit none
    private
    public :: is_infinity, is_nan, is_negative, is_zero

    interface is_infinity
        !! Is the value a positive or negative infinity?
        module procedure is_infinity_real
        module procedure is_infinity_double
    end interface

    interface is_nan
        !! Is the value not a number?
        module procedure is_nan_real
        module procedure is_nan_double
    end interface

    interface is_negative
        !! Is the value negative, including negative zero or infinity?
        module procedure is_negative_real
        module procedure is_negative_double
    end interface

    interface is_zero
        !! Is the value positive or negative zero?
        module procedure is_zero_real
        module procedure is_zero_double
    end interface
contains
    elemental function is_infinity_real(val) result(is_infinity)
        real, intent(in) :: val
        logical :: is_infinity


        if (is_nan(val)) then
            is_infinity = .false.
        else
            ! This isn't quite right. It is conceivable for a processor
            ! to have numbers greater than huge that aren't considered infinity,
            ! but for now I don't know of a reliable way to test for that
            is_infinity = val < -huge(val) .or. val > huge(val)
        end if
    end function

    elemental function is_infinity_double(val) result(is_infinity)
        double precision, intent(in) :: val
        logical :: is_infinity


        if (is_nan(val)) then
            is_infinity = .false.
        else
            ! This isn't quite right. It is conceivable for a processor
            ! to have numbers greater than huge that aren't considered infinity,
            ! but for now I don't know of a reliable way to test for that
            is_infinity = val < -huge(val) .or. val > huge(val)
        end if
    end function

    elemental function is_nan_real(val) result(is_nan)
        real, intent(in) :: val
        logical :: is_nan

        is_nan = .not.(val >= 0.0 .or. val <= 0.0)
    end function

    elemental function is_nan_double(val) result(is_nan)
        double precision, intent(in) :: val
        logical :: is_nan

        is_nan = .not.(val >= 0.d0 .or. val <= 0.d0)
    end function

    elemental function is_negative_real(val) result(is_negative)
        real, intent(in) :: val
        logical :: is_negative

        if (is_nan(val)) then
            is_negative = .false.
        else
            is_negative = sign(1.0, val) < 0.0
        end if
    end function

    elemental function is_negative_double(val) result(is_negative)
        double precision, intent(in) :: val
        logical :: is_negative

        if (is_nan(val)) then
            is_negative = .false.
        else
            is_negative = sign(1.d0, val) < 0.d0
        end if
    end function

    elemental function is_zero_real(val) result(is_zero)
        real, intent(in) :: val
        logical :: is_zero

        if (is_nan(val)) then
            is_zero = .false.
        else
            is_zero = .not. (val > 0.0 .or. val < 0.0)
        end if
    end function

    elemental function is_zero_double(val) result(is_zero)
        double precision, intent(in) :: val
        logical :: is_zero

        if (is_nan(val)) then
            is_zero = .false.
        else
            is_zero = .not. (val > 0.d0 .or. val < 0.d0)
        end if
    end function
end module
