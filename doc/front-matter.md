---
project: exceptional_numbers
summary: Helpers that deal with exceptional numbers properly
project_website: https://gitlab.com/everythingfunctional/exceptional_numbers
project_download: https://gitlab.com/everythingfunctional/exceptional_numbers/-/releases
author: Brad Richardson
email: everythingfunctional@protonmail.com
website: https://everythingfunctional.com
twitter: https://twitter.com/everythingfunct
github: https://github.com/everythingfunctional
src_dir: ../src
display: public
         protected
         private
sort: permission-alpha
output_dir: ../public
graph: true
...

This module defines a library of helper functions for working with floating point values.
Specifically, it appropriately handles NaN, infinity and negative zero.