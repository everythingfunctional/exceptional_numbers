# exceptional_numbers

A library for easily and safely querying floating point values with regards to "special" values.
Namely, `NaN` and `Inf` are considered as possible values.

Included functions:

* `is_infinity` - returns `.true.` if value is positive or negative infinity
* `is_nan` - returns `.true.` if value is not a number (I.e. `NaN`).
  **NOTE**: If this function returns `.true.` for a value, then no other function will return `.true.` for that value
* `is_negative` - returns `.true.` if the value is not positive, including for `-Inf` and `-0.0`
* `is_zero` - returns `.true.` if and only if the value is exactly `0.0` or `-0.0`

Note that these functions all avoid exact equality comparisons so as to allow for compiling with all warnings and errors turned on.
